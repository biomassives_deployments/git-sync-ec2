#!/bin/sh

# config settings
# "EC2 instance id" "url config base" branch protocol user "path to git repository"
# See GIT URLS under git help push for mrore info on url config base

id_file=$(git config --get sync-ec2.identity-file)
user=$(git config --get sync-ec2.user)
repo=$(git config --get sync-ec2.repository-path)

# start the EC2 instance
git-sync-ec2-instance-start.sh
name=$(git-sync-ec2-instance-name.sh)

# add a temporary ssh config with our EC2 host name
ssh_config=git-sync-ec2-$RANDOM
storm add --id_file=$id_file $ssh_config "$name" $user

# delay a small moment to give the EC2 instance time to finish spinning up
sleep 10s

# push to the EC2 instance
git pull $@ ssh://$ssh_config/$repo master

# stop the EC2 instance
git-sync-ec2-instance-stop.sh

# delete the temporary ssh config
storm delete $ssh_config
